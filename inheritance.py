"""Type of inheritance:
      1. single inheritance [1 to 1 relationship]
      2. multi level inheritance [1 to many relationship..ex: father-->son-->grandson]
      3. hierarchical inheritance     father
                                        / \           [from father have more then one child, properties are going to his son and daughter]
                                     son   daughter
      4. multiple inheritance       father mother
                                      \     /         [from father and mother both properties are going to  child]
                                       child

"""

# code snippet for single inheritance in python
print("single inheritance.... line no 16 to 28")


class father:
    def work(self):
        print("successful MAN")


class son(father):
    def occupation(self):
        self.work()
        print("successful software engineer")


obj1 = son()
obj1.occupation()

print (
    "-----------------------------------------------------------------------------------------------------------------")

# code snippet for multi level inheritance
print("multi level inheritance......line no 35 to 53")
"""in multi level inheritance classes can be inherit properties from their parent class"""


class father:
    def method1(self):
        print ("it is a parent method")


class son(father):
    def method2(self):
        print("it is a child method")


class grandson(father):
    def method3(self):
        print ("it is a sub child method")


obj2 = grandson()

obj2.method1()
obj2.method3()

print("-----------------------------------------------------------------------------------------------------------")

# hierarchical inheritance

print ("hierarchical inheritance....from line no 61 to 78 ")
""" In hierarchical inheritance multiple classes can inherit properties in between them """


class father:
    def method1(self):
        print ("it is a parent method")


class son(father):
    def method2(self):
        print("it is a child method")


class grandson(son):
    def method3(self):
        print ("it is a sub child method")


obj3 = grandson()
obj3.method3()
obj3.method2()
obj3.method1()

print("------------------------------------------------------------------------------------------------------")

# multiple inheritance
print (" multiple inheritance from line no 93 ")


class father:
    def method1(self):
        print ("father giving properties to child ")


class mother:
    def method2(self):
        print("mother giving properties to child")


class child(father, mother):
    def __init__(self):
        pass

    def method3(self):
        print("trying to get better day by day")


obj4 = child()
obj4.method3()
obj4.method2()
obj4.method1()


print("-----------------------------------------END OF INHERITANCE----------------------------------------------------")
