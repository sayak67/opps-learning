class fish:
    """how to build constructor in python , constructor is used to decliar variable inside the class"""

    def __init__(self, type, name):
        self.type = type
        self.name = name
        self.food = self.type + " " + "small fishes"
        print("constructor is calling")

    def aquariumSize(self):
        if self.type == "saltwater":
            print("large size of aquarium needed")
        elif self.type == "freshwater":
            print("size of the aquarium depends upon the type of fish you choose")

    """way of creating methods in python , methods mainly used for logic building of your application """
    def skimmer(self):
        if self.type =="saltwater":
            print("skimmer needed")
        elif self.type =="freshwater":
            print("if you have budget permit add a surface skimmer")
        else:
            print("you can add a top filter to clear the water darts for any of a kind aquarium")


""" this way you can create a instance method of a class in python
    A class is a user-defined blueprint or prototype from which objects are created.
    Classes provide a means of bundling data and functionality together.
    Creating a new class creates a new type of object, allowing new instances of that type to be made.
    Each class instance can have attributes attached to it for maintaining its state. 
    Class instances can also have methods (defined by its class) for modifying its state.
    Instance Method::::::::::::::______
    Instance attributes are those attributes that are not shared by objects. 
    Every object has its own copy of the instance attribute.

"""



"""how to create object of class in python and how to pass parametar value to object of the class"""


# obj1 = fish("saltwater", "stingray")
# obj2 = fish("freshwater", "dolphin")
#print(obj1.food)
"""here we are calling the instance method after createing a object of the class then call the instance method inside class """
obj3= fish("saltwater","pufferfish")
obj3.aquariumSize()
obj3.skimmer()
